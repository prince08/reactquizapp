import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';

export const CircleLoader = (props) => {
  return <CircularProgress {...props}/>;
};

export const LinearLoader = (props) => {
  return <LinearProgress {...props}/>;
};