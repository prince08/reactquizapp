import React from "react";
import MobileStepper from "@material-ui/core/MobileStepper";
import { Button } from "./button";

export const Stepper = (props) => {
  const { maxSteps, activeStep, handleBack, handleNext } = props;
  return (
    <MobileStepper
      variant="progress"
      steps={maxSteps}
      position="static"
      activeStep={activeStep}
      backButton={
        <Button
          text={'Back'}
          variant={'text'}
          size="small"
          onPress={handleBack}
          disabled={activeStep === 0}
        />
      }
      nextButton={
        <Button
          text={'Next'}
          variant={'text'}
          size="small"
          onPress={handleNext}
          disabled={activeStep === maxSteps - 1}
        />
      }
    />
  )
};

export default Stepper;
