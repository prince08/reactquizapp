import React from "react";
import { PAGE_NOT_FOUND } from "../utilities/constants";

const NoMatch = () => {
  return (
    <div className='f1 jCenter p24'>
      <h1 className='head'>{PAGE_NOT_FOUND}</h1>
    </div>
  );
};

export default NoMatch;