import React from 'react';
import objectGet from 'lodash/get';
import './style.css';
import { Button } from "../../commonComponents";

export const EndScreen = (props) => {
  const { questions, score, history } = props;
  const scoreStr = `You scored ${score} out of ${questions.length}`;
  return (
    <div className='f1 fdc m20'>
      <div className={'subHead'}>{scoreStr}</div>
      <Button text={'Play Again'} onPress={() => history.replace('quiz')}/>
      <table>
        <tbody>
        <tr>
          <th>Question</th>
          <th>Correct answer</th>
          <th>Submitted answer</th>
        </tr>
        {
          questions.map((ques, index) => (
            <ListRow key={objectGet(ques, 'question', index)} data={ques}/>
          ))
        }
        </tbody>
      </table>
    </div>
  )
};

const ListRow = ({ data }) => {
  const { question, correct_answer, submitted_answer } = data;

  return (
    <tr>
      <td dangerouslySetInnerHTML={{ __html: question }}/>
      <td>{correct_answer}</td>
      <td>{submitted_answer}</td>
    </tr>
  )
};

export default EndScreen;
  