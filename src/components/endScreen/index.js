import { connect } from 'react-redux';
import EndScreen from "./endScreen";

const mapStateToProps = ({ questions, score }) => ( { score, questions } );
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(EndScreen);
