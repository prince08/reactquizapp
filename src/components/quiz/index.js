import { connect } from 'react-redux';
import Quiz from "./quiz";
import { getQuestionsList, handleQuizSubmit } from '../../redux/actions/index';

const mapStateToProps = ({ loading, questions }) => ( { loading, questions } );
const mapDispatchToProps = { getQuestionsList, handleQuizSubmit };

export default connect(mapStateToProps, mapDispatchToProps)(Quiz);
