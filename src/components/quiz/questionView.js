import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

export default (props) => {
  const {
    question, index, value, onSubmitAnswer,
    answers = [],
  } = props;

  return (
    <div className='f1 fdc'>
      <div className='f1 fdc m20 aCenter subHead' dangerouslySetInnerHTML={{ __html: question }}/>
      <div className='f1 fdc m20 aCenter'>
        <RadioGroup
          className={'f fdr'}
          value={value}
          onChange={({ target: { value } }) => onSubmitAnswer(index, value)}
        >
          {
            answers.map(ans => (
              <FormControlLabel
                key={ans}
                value={ans}
                control={<Radio/>}
                label={<div dangerouslySetInnerHTML={{ __html: ans }}/>}
              />
            ))
          }
        </RadioGroup>
      </div>
    </div>
  )
};
