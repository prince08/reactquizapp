import React from 'react';
import { AppBar } from 'material-ui';
import objectGet from 'lodash/get';

import { APP_TITLE } from '../../utilities/constants';
import { Button, CircleLoader, MobileStepper } from '../../commonComponents';
import QuestionView from './questionView';

export default class Quiz extends React.PureComponent {
  state = {
    activeStep: 0,
    submittedAnswers: {},
  };

  handleNext = () => {
    this.setState(prevState => ( {
      activeStep: prevState.activeStep + 1,
    } ));
  };

  handleBack = () => {
    this.setState(prevState => ( {
      activeStep: prevState.activeStep - 1,
    } ));
  };

  onSubmitAnswer = (index, value) => {
    this.setState(({ submittedAnswers }) => {
      return { submittedAnswers: { ...submittedAnswers, [index]: value } }
    });
  };

  handleSubmit = () => {
    const { questions, handleQuizSubmit, history } = this.props;
    const { submittedAnswers } = this.state;
    const totalQuestions = questions.length;
    const totalSubmittedAnswers = Object.keys(submittedAnswers).length;

    if (totalQuestions !== totalSubmittedAnswers) {
      return alert(`You have ${totalQuestions - totalSubmittedAnswers} unanswered question, Please provide answer to those unanswered questions`);
    }

    return handleQuizSubmit(questions, submittedAnswers, () => {
      history.replace('result');
    });
  };

  componentDidMount() {
    const { getQuestionsList } = this.props;
    getQuestionsList();
  }

  render() {
    const { questions, loading } = this.props;
    const maxSteps = questions.length;
    const { activeStep, submittedAnswers } = this.state;
    const selectedQuestion = objectGet(questions, `[${activeStep}]`, {});
    const selectedQuestionValue = objectGet(submittedAnswers, activeStep, '');

    if (loading && questions.length <= 0) {
      return (
        <div className='f1 fdc aCenter m20'>
          <CircleLoader/>
        </div>
      )
    }
    return (
      <div className='f1 fdc'>
        <AppBar
          className={"dashboard-header"}
          title={APP_TITLE}
          iconElementLeft={<div/>}
        />
        <div className='f1 fdc m20'>
          <QuestionView
            {...selectedQuestion}
            value={selectedQuestionValue}
            index={activeStep}
            onSubmitAnswer={this.onSubmitAnswer}
          />
          <MobileStepper
            maxSteps={maxSteps}
            activeStep={activeStep}
            handleNext={this.handleNext}
            handleBack={this.handleBack}
          />
        </div>
        <div className='f1 fdc m20 aCenter'>
          <Button
            text={'Submit'}
            onPress={this.handleSubmit}
            disabled={Object.keys(submittedAnswers).length < maxSteps}
          />
          <div className={'mt10 fcBrown'}>
            You need to answer all the question to enable submit button.
          </div>
        </div>
      </div>
    )
  }
}
