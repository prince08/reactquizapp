import React from 'react';
import { AppBar } from 'material-ui';

import { APP_TITLE } from '../../utilities/constants';
import { Button } from '../../commonComponents';

export default ({ history }) => {

  return (
    <div className='f1 fdc'>
      <AppBar
        className={"dashboard-header"}
        title={APP_TITLE}
        iconElementLeft={<div/>}
      />
      <div className='f1 fdc m20 aCenter'>
        This is a sample quiz application, Press "Start" to start the quiz.
      </div>
      <div className='f1 fdc m20 aCenter'>
        <Button text={'Start Quiz'} onPress={() => history.push('quiz')}/>
      </div>
    </div>
  )
};
