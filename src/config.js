export default {
  SERVER_URL: 'https://opentdb.com/api.php',
  QUIZ_QUESTIONS_URL: 'https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean',
}