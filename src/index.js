import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { getMuiTheme, lightBaseTheme, MuiThemeProvider } from 'material-ui/styles';

import './style/index.css';
import { NoMatch } from "./commonComponents";
import Welcome from "./components/welcome";
import Quiz from "./components/quiz";
import EndScreen from "./components/endScreen";
import store from './redux/store';

const Main = () => (
  <Provider store={store}>
    <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Welcome}/>
          <Route exact path="/welcome" component={Welcome}/>
          <Route exact path="/quiz" component={Quiz}/>
          <Route exact path="/result" component={EndScreen}/>
          <Route component={NoMatch}/>
        </Switch>
      </BrowserRouter>
    </MuiThemeProvider>
  </Provider>
);

ReactDOM.render(<Main/>, document.getElementById('root'));