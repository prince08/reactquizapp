import objectGet from 'lodash/get';
import config from '../../config';
import { get } from '../../utilities/api';
import { isOkResponse } from "../../utilities/validations";
import { ADD_QUESTIONS, TOGGLE_LOADER, ADD_SCORE, } from "../reduxConstants";

const toggleLoader = (show = true) => ( { type: TOGGLE_LOADER, data: show } );

export const getQuestionsList = () => {
  return async (dispatch) => {
    try {
      dispatch(toggleLoader(true));
      let { data: { results }, status } = await get(config.QUIZ_QUESTIONS_URL);
      dispatch(toggleLoader(false));
      if (isOkResponse(status)) {
        results = results.map(item => {
          const { correct_answer, incorrect_answers = [], } = item;
          let answers = [...incorrect_answers];
          const randomIndex = Math.floor(Math.random() * answers.length);
          answers.splice(randomIndex, 0, correct_answer);
          item.answers = answers;
          return item;
        });
        return dispatch({ type: ADD_QUESTIONS, data: results });
      }
      return dispatch({ type: ADD_QUESTIONS, data: [] });
    } catch (err) {
      dispatch(toggleLoader(false));
      console.log("-inside error getQuestionsList :- ", err);
      return dispatch({ type: ADD_QUESTIONS, data: [] });
    }
  };
};

export const handleQuizSubmit = (questions, submittedAnswers, cb) => {
  return dispatch => {
    let score = 0;
    questions = questions.map((ques, index) => {
      const { correct_answer } = ques;
      if (correct_answer === objectGet(submittedAnswers, index, '')) {
        score++;
      }
      return {
        ...ques,
        submitted_answer: objectGet(submittedAnswers, index, '')
      }
    });
    dispatch({ type: ADD_SCORE, data: score });
    dispatch({ type: ADD_QUESTIONS, data: questions });
    return cb();
  }
};