import { ADD_QUESTIONS, TOGGLE_LOADER, ADD_SCORE } from "../reduxConstants";

export default function (state = {}, action) {
  const { type, data } = action;

  switch (type) {
    case ADD_QUESTIONS:
      return { ...state, questions: data };
    case TOGGLE_LOADER:
      return { ...state, loading: data };
    case ADD_SCORE:
      return { ...state, score: data };
    default:
      return state;
  }
}

