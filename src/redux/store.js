import { applyMiddleware, compose, createStore } from "redux";
import reducer from "./reducers/index";
import thunk from "redux-thunk";

const INITIAL_STATE = {
  questions: [],
  loading: false,
  score: 0,
};
const enhancer = compose(applyMiddleware(thunk));

const store = createStore(reducer, INITIAL_STATE, enhancer);

export default store;
