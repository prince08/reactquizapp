import axios from 'axios';
import config from '../config';

export const get = handleApiCall.bind(null, 'GET');
export const post = handleApiCall.bind(null, 'POST');
export const put = handleApiCall.bind(null, 'PUT');
export const del = handleApiCall.bind(null, 'DELETE');

function handleApiCall(method, url, data = {}, otherOptions = {}) {
  const options = {
    method,
    timeout: 5000,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    ...otherOptions
  };

  if (method !== 'GET') {
    options.data = data;
  }

  if (url.toLowerCase().indexOf('http') < 0) {
    url = config.SERVER_URL + url;
  }

  options.url = url;

  return axios(options);
}